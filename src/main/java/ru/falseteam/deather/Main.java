package ru.falseteam.deather;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("App");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        Scene scene = new Scene(grid, 500, 300);
        primaryStage.setScene(scene);
        primaryStage.show();
        Text scenetitle = new Text("Make by Deather");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0);
//        angle in degrees
        Label aid = new Label("���� � ��������:");
        grid.add(aid, 0, 1);
        ComboBox aidField = new ComboBox();
        aidField.setPromptText("�������� ����");
        int i = 0;
        while (i < 95) {
            aidField.getItems().addAll(Integer.toString(i));
            i = i + 5;
        }
        grid.add(aidField, 1, 1);
//        angle in radians
        Label air = new Label("���� � ��������:");
        grid.add(air, 0, 2);
        TextField airField = new TextField();
        grid.add(airField, 1, 2);
//        result
        Label result = new Label("���������:");
        grid.add(result, 0, 3);
        TextField resultField = new TextField();
        grid.add(resultField, 1, 3);
        Button sin = new Button("sin");
        grid.add(sin, 0, 4);
        Button cosbtn = new Button("cos");
        grid.add(cosbtn, 1, 4);
        Button tgbtn = new Button("tg");
        grid.add(tgbtn, 2, 4);

        cosbtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                String a = String.valueOf(aidField.getValue());
                if (aidField.getValue() == null) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText(null);
                    alert.setContentText("�������� ����");
                    alert.showAndWait();
                } else {
                    double test = Double.parseDouble(a);
                    double rad = Math.toRadians(test);
                    double sin = Math.cos(rad);
                    String Srad = String.valueOf(rad);
                    String Rsin = String.valueOf(sin);
                    if (Srad.length() > 7) {
                        airField.setText(Srad.substring(0, 7));
                    } else {
                        airField.setText(Srad);
                    }
                    if (Rsin.length() > 7) {
                        resultField.setText(Rsin.substring(0, 7));
                    } else {
                        resultField.setText(Rsin);
                    }
                }
            }
        });

        tgbtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                String a = String.valueOf(aidField.getValue());
                if (aidField.getValue() == null) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText(null);
                    alert.setContentText("�������� ����");
                    alert.showAndWait();
                } else {
                    double test = Double.parseDouble(a);
                    double rad = Math.toRadians(test);
                    double sin = Math.tan(rad);
                    String Srad = String.valueOf(rad);
                    String Rsin = String.valueOf(sin);
                    if (Srad.length() > 7) {
                        airField.setText(Srad.substring(0, 7));
                    } else {
                        airField.setText(Srad);
                    }
                    if (Rsin.length() > 7) {
                        resultField.setText(Rsin.substring(0, 7));
                    } else {
                        resultField.setText(Rsin);
                    }
                }
            }
        });

        sin.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                String a = String.valueOf(aidField.getValue());
                if (aidField.getValue() == null) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText(null);
                    alert.setContentText("�������� ����");
                    alert.showAndWait();
                } else {
                    double test = Double.parseDouble(a);
                    double rad = Math.toRadians(test);
                    double sin = Math.sin(rad);
                    String Srad = String.valueOf(rad);
                    String Rsin = String.valueOf(sin);
                    if (Srad.length() > 7) {
                        airField.setText(Srad.substring(0, 7));
                    } else {
                        airField.setText(Srad);
                    }
                    if (Rsin.length() > 7) {
                        resultField.setText(Rsin.substring(0, 7));
                    } else {
                        resultField.setText(Rsin);
                    }
                }
            }
        });
    }
}
